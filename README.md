# Moodle Korrektur Uploader
Mit diesem Programm kannst du automatisch die Bewertungstabelle aus dem Moodle ausfüllen lassen, ebenso wird ein Zip Archiv erstellt, dass alle korrigierten Abgaben erhält welches man bei Moodle hochladen kann.

## Installation
1. Lade die [PunkteInCSV.jar](https://git.rwth-aachen.de/leander.schulten/moodle-korrektur-uploader/-/raw/master/PunkteInCSV.jar) Datei herunter.
2. Kopiere die Datei an eine Position wo sie bleiben kann
3. Öffne ein Terminal an in diesem Ordner
4. Führe `java -jar PunkteInCSV.jar alias_as moodle` aus (`moodle` kannst du durch irgendwas ersetzen)
5. Führe den Befehl aus der dir das Program gesagt hat als du den alias erstellt hast
6. Du kannst nun in jedem Ordner `moodle` aufrufen um das Program zu starten (Oder wie du den alias stattdessen genannt hast)

## Einrichten für ein Tutorium
1. Ordner für das Tutorium erstellen
2. Erstelle eine `groups.txt` Datei
3. Trage die Abgabegruppen in die groups.txt Datei ein (Meistens nur beim ersten mal Korrigieren notwendig, weil sich die Abgabegruppen danach sich ja in der Regel nicht mehr ändern). Das Format als Regex ist:  
        `group ([0-9]+ )+`  
        Die Datei sieht dann z.B. so aus:  
        `group 123542 982132 345234`  
        `group 345657 324756`  
4. Trage alle Leute in die `groups.txt` Datei ein (nur notwendig, wenn du keine Bewertungstabelle herunterladen kannst (siehe #Vorgensweise 1.x) )  
        1. Öffne die Übersieht beschrieben in #Vorgensweise 1.x  
        2. Öffne die Entwicklerkonsole deines Brwosers und führe dort  
        `s = '\n';for(let row of document.getElementsByClassName('unselectedrow')){s += 'name ' + row.children[2].innerText + ' ' + row.children[3].innerText + '\n'; } s`  
        aus  
        3. Kopiere die `name ... `Einträge in die `groups.txt`

## Vorgehensweise beim Korrigieren
1. Alle benötigen Daten runterladen:  
    1. Öffne auf Moodle den Kurs bei dem zu Tutor bist
    2. Gehe auf Übungsblätter und wähle das Übungsblatt aus welches du korrigieren möchtest
    3. Wähle deine Übungsgruppe unter "Getrennte Gruppen" aus 
    4. Klicke auf "Alle Abgaben anzeigen"
    5. Wähle ganz oben unter Bewertungsvorgang "Alle Abgaben herunterladen", wenn möglich auch "Bewertungstabelle herunterladen" (Wenn dies nicht möglich ist, bitte den Assistenten der Übung die Option Offline-Bewertungstabelle in den Feedback-Typen der Übungen zu aktivieren)
2. Alle Abgaben korrigieren:  
    1. Entpacke die heruntergeladenen Abgaben
    3. Für jede Abgabe:  
        - Korrigiere die Abgabe und füge die Punktzahl im Dateinamen hinzu (Vor .pdf, getrennt durch ein `_` und ein `,` für Dezimalzahlen). Z.B: "abgabe.pdf" => "abgabe_14,5.pdf",  "test_1.pdf" => "test_1_10.pdf"  
3. Alle Korrekturen hochladen:
    1. Öffne die Kommandozeile bei deinen Korrekturen  
    2. In der Kommandezeile: `moodle create` (`moodle` durch deinen alias namen ersetzen wenn du in # Installation Punkt 4 einen anderen Namen verwendet hast)  
    3. Schritt 1.1 - 1.4 durchführen  
    4. (Wenn möglich) Nun ganz oben unter Bewertungsvorgang "Bewertungstabelle hochladen" auswählen und die generierte CSV-Datei auswählen(Endet mit "_mitBewertung") und den Hacken bei "Update von Datensätzen zulassen, die seit dem letzten Upload angepasst wurden." setzten.  
    5. (Alternativ) Die Liste nach Matrikelnummern sortieren (die älteste ganz oben). Dann kannst du alle Bewerten (Punktezahl hast du in Punkt 2 bekommen)  
    6. Nun unter Bewertungsvorgang "Mehrere Feedbackdateien in einer Zip-Datei hochladen" auswählen und die Zip-Datei "zipForMoodle.zip" auswählen
   

## Dokumentation des Programs
Das Programm kann mit verschiedenen Befehlen gestartet werden.

#### Ohne Befehl
Es wird ein Hilfstext angezeigt, der die anderen Befehle auflistet

#### Befehl `alias_as`
Das Program erwartet nun einen weiteren Paramter, nämlich den Namen des aliases. Z.b. `java -jar PunkteInCSV.jar alias_as moodle`.
Willst du den alias wieder Löschen, Öffne die Datei `~/.bash_profile` (macOS) oder `~/.bashrc` (linux) und lösche die Zeile `alias moodle='java -jar ...'` (_moodle_ kann auch was anderes sein wenn du den alias anders genannt hast)

#### Befehl `create`
Der Befehlt kann drei Optionale Parameter (Pfade) bekommen. Sind diese nicht gegeben, sucht das Program selber nach diesen Pfaden.
- **pfad/zu/files/**: Dieser Paramter wird daran erkannt, dass der Pfad auf ein Verzeichnis zeigt.  
                         Default: Liegen im aktuellen Order Dateien oder Ordner die im Namen `_assignsubmission_file_` enthalten, dann ist der aktuelle Ordner der Ordner in dem die Abgaben liegen.
                         Das gleiche passiert mit allen Unterverzeichnissen des aktuellen Verzeichnisses.
- **pfad/zu/groups.txt**: Dieser Paramter wird daran erkannt, dass der Pfad mit `groups.txt` aufhört, andere Dateinamen werden nicht unterstützt.  
                         Default: Es wird im aktuellen Verzeichnis und im jedem Verzeichnis bis `/` nach einer `groups.txt` Datei gesucht.
                         Das gleiche passiert auch ausgehend vom Verzeichnis wo die Abgabe liegen (Dateien oder Ordner mit dem Namen `*_assignsubmission_file_*`)
- **pfad/zu/Bewertungs\*.csv**: Dieser Paramter wird daran erkannt, dass der Pfad mit `.csv` aufhört, andere Dateinamen werden nicht unterstützt.  
                         Default: Es wird im aktuellen Verzeichnis nach einer `Bewertungs\*.csv` Datei gesucht, die aber nicht mit `_MitBewertung.csv` aufhört.
                         Das gleiche passiert auch im Verzeichnis wo die Abgabe liegen (Dateien oder Ordner mit dem Namen `*_assignsubmission_file_*`)
