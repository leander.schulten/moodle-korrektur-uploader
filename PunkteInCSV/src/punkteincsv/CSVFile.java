/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punkteincsv;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.UnaryOperator;

/**
 *
 * @author leanderSchulten
 */
public class CSVFile {

    private String header;
    private List<String[]> csvLines = new ArrayList<>();
    private List<Student> students = new ArrayList<>();
    private Path csv;

    static class Student {

        String matrikelnummer;
        String abgabeId;
        String name;

        public Student(String matrikelnummer, String abgabeId, String name) {
            this.matrikelnummer = matrikelnummer;
            this.abgabeId = abgabeId;
            this.name = name;
        }

        @Override
        public String toString() {
            return name + " " + matrikelnummer;
        }

    }

    private static String[] parseCSVLine(String line) {
        List<String> csvData = new ArrayList<>();
        int start = 0;
        boolean inString = false;
        boolean wasInString = false;
        for (int i = 0; i < line.length(); i++) {
            char currentChar = line.charAt(i);
            if (currentChar == '"') {
                inString = !inString;
                wasInString = true;
            }
            if (!inString && currentChar == ',') {
                if (wasInString) {
                    csvData.add(line.substring(start + 1, i - 1));
                } else {
                    csvData.add(line.substring(start, i));
                }
                wasInString = false;
                start = i + 1;
            }
        }
        csvData.add(line.substring(start, line.length()));        
        return csvData.toArray(new String[csvData.size()]);
    }

    private static byte[] csvDataToString(String[] csvData) {
        StringBuilder builder = new StringBuilder(200);
        for (String string : csvData) {
            if (string == null) {
                builder.append(',');
            } else {
                boolean withSpaces = (string.contains(" ") || string.contains(",")) && !(string.startsWith("\"") && string.endsWith("\""));
                if (withSpaces) {
                    builder.append('"');
                }
                builder.append(string);
                if (withSpaces) {
                    builder.append('"');
                }
                builder.append(',');
            }
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append("\n");
        return builder.toString().getBytes();
    }

    public CSVFile(Path csv) throws IOException {
        parse(csv);
    }

    private void parse(Path csv) throws IOException {
        List<String> lines = Files.readAllLines(csv);
        this.csv = csv;
        boolean lineOne = true;
        for (String line : lines) {
            String[] csvData = parseCSVLine(line);
            if (lineOne) {
                lineOne = false;
                if (csvData.length < 5 || !csvData[2].equals("Matrikelnummer") || !csvData[4].equals("Bewertung") || !csvData[1].equals("Vollständiger Name")) {
                    if (csvData.length < 5) {
                        System.err.println("Die CSV Datei hat zu wenig Spalten. " + csv);
                    } else if (!csvData[2].equals("Matrikelnummer")) {
                        System.err.println("Die Spalte mit Index 3 heißt nicht 'ID-Nummer' sondern '" + csvData[2] + "'");
                    } else if (!csvData[4].equals("Bewertung")) {
                        System.err.println("Die Spalte mit Index 5 heißt nicht 'Bewertung' sondern '" + csvData[4] + "'");
                    } else if (!csvData[1].equals("Vollständiger Name")) {
                        System.err.println("Die Spalte mit Index 1 heißt nicht 'Vollständiger Name' sondern '" + csvData[1] + "'");
                    }
                    throw new RuntimeException("PunkteCSVDatei hat neues/falsches Format!");
                }
                header = line;
            } else {
                // get group for moodleID
                String abgabeId = csvData[0].substring(csvData[0].lastIndexOf('n') + 1);
                students.add(new Student(csvData[2], abgabeId, csvData[1]));
                csvLines.add(csvData);
            }
        }
    }

    public List<Student> getStudents() {
        return students;
    }

    public String getAbgabeIdForMatrikelnummer(String matrikelnummer) {
        for (Student student : students) {
            if (student.matrikelnummer.equals(matrikelnummer)) {
                return student.abgabeId;
            }
        }
        return null;
    }

    public Student getStudentForAbgabeId(String abgabeId) {
        for (Student student : students) {
            if (student.abgabeId.equals(abgabeId)) {
                return student;
            }
        }
        return null;
    }

    public void write(UnaryOperator<String> getPointsForMatrNummer) throws FileNotFoundException, IOException {
        final String newLastChanged = '"' + LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE, d. MMM u, k:m", Locale.GERMAN)) + '"';

        String filename = csv.getFileName().toString();
        String newFilename = filename.substring(0, filename.length() - 4) + "_MitBewertung.csv";
        try (FileOutputStream output = new FileOutputStream(newFilename)) {
            output.write(header.getBytes());
            output.write('\n');
            for (String[] csvData : csvLines) {
                String b = getPointsForMatrNummer.apply(csvData[2]);
                final int BEWERTUNGS_INDEX = 4;
                if (b == null) {
                    System.out.println("Student " + csvData[1] + " " + csvData[2] + " hat keie Punkte bekommen");
                    System.out.println(csvData[BEWERTUNGS_INDEX]);
                    if (csvData[BEWERTUNGS_INDEX].length() == 0) {
                        csvData[BEWERTUNGS_INDEX] = "\"0,0\"";
                    }
                } else {
                    if (csvData[BEWERTUNGS_INDEX].length() > 0 && !csvData[BEWERTUNGS_INDEX].equals(b)) {
                        System.out.println("Punkte von Student " + csvData[1] + " " + csvData[2] + " werden von + " + csvData[BEWERTUNGS_INDEX] + " zu " + b + " geändert");
                    }
                    csvData[BEWERTUNGS_INDEX] = b;
                }
                csvData[8] = newLastChanged;
                output.write(csvDataToString(csvData));
            }
        }
    }
}
