/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package punkteincsv;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 *
 * @author leanderSchulten
 */
public class PunkteInCSV {

    static List<Set<String>> groups = new ArrayList<>();
    static Map<Set<String>, String> punkte = new HashMap<>();
    static Map<String, String> matrForName = new HashMap<>();
    static Map<String, String> fullNameForMatrNum = new HashMap<>();

    static Set<String> getGroupForMatrNummer(String num) {
        for (Set<String> group : groups) {
            if (group.contains(num)) {
                return group;
            }
        }
        System.out.println("No group for Matrnum : " + num);
        Set<String> set = new HashSet<>();
        set.add(num);
        return set;
    }

    static Set<String> getGroupForName(String moodleId) {
        String matNum = matrForName.get(moodleId);
        if (matNum == null) {
            System.err.println("No MatrNumber for name " + matNum);
            return null;
        }
        return getGroupForMatrNummer(matNum);
    }

    static String getPointsForMatrNummer(String matrNummer) {
        String s = punkte.get(getGroupForMatrNummer(matrNummer));
        if (s != null && !(s.contains(",") || s.contains("."))) {
            s += ".0";
        }
        System.out.println("points for " + matrNummer + " " + s);
        return s != null ? "\"" + s.replace('.', ',') + "\"" : null;
    }

    static void parseGroup(String line) {
        StringTokenizer tok = new StringTokenizer(line, " ");
        if (tok.countTokens() < 2) {
            throw new RuntimeException("Zeile hat falsches Format für Gruppe'group (<matrNummer> )+'  : " + line);
        }
        HashSet<String> set = new HashSet<String>();
        tok.nextToken();
        while (tok.hasMoreElements()) {
            String matrNummer = tok.nextToken();
            for (char c : matrNummer.toCharArray()) {
                if (!Character.isDigit(c)) {
                    System.err.println(" Keine gültige Matrikelnummer : <" + matrNummer + ">");
                    break;
                }
            }
            set.add(matrNummer);
        }
        groups.add(set);
    }

    static void parseName(String line) {
        StringTokenizer tok = new StringTokenizer(line, " ");
        if (tok.countTokens() < 3) {
            throw new RuntimeException("Zeile hat falsches Format für Name 'name <name> <matrNummer>'  : " + line);
        }
        tok.nextToken();
        String middle = "";
        String matNr = null;
        while (true) {
            String next = tok.nextToken();
            if (!tok.hasMoreTokens()) {
                matNr = next;
                break;
            }
            middle += " " + next;
        }
        middle = middle.substring(1);
        String lastName = middle;
        int index = lastName.indexOf(',');
        if (index >= 0) {
            lastName = lastName.substring(0, index);
        }
        matrForName.put(lastName, matNr);
        fullNameForMatrNum.put(matNr, middle);
    }

    static void checkGroups() {
        groups.forEach((set) -> {
            set.forEach((t) -> {
                groups.forEach((s) -> {
                    if (s != set) {
                        if (s.contains(t)) {
                            throw new RuntimeException("Student " + t + " is in two groups");
                        }
                    }
                });
            });
        });
    }

    static void parsePointsForGroup(String line) {
        StringTokenizer tok = new StringTokenizer(line, " ");
        if (tok.countTokens() != 2) {
            throw new RuntimeException("Zeile hat falsches Format für Pukte '<matrNummer> <punkte>'  : " + line);
        }
        String matrNummer = tok.nextToken();
        String pun = tok.nextToken();
        punkte.put(getGroupForMatrNummer(matrNummer), pun);
    }

    static class AbgabeIdGroups {

        private Map<String, Set<String>> abgabeIdToGroup;
        private CSVFile csvFile;

        AbgabeIdGroups(List<Set<String>> matrikelnummerGroups, CSVFile csvFile) {
            this.abgabeIdToGroup = new HashMap<>(matrikelnummerGroups.size());
            this.csvFile = csvFile;
            matrikelnummerGroups.forEach(group -> {
                Set<String> abgabeIdGroup = new HashSet<>(group.size());
                group.forEach(m -> {
                    String abgabeId = csvFile.getAbgabeIdForMatrikelnummer(m);
                    abgabeIdGroup.add(abgabeId);
                    abgabeIdToGroup.put(abgabeId, abgabeIdGroup);
                });
            });
        }

        List<String> getOtherAbgabeIdsForAbgabeId(String abgabeId) {
            Set<String> group = abgabeIdToGroup.get(abgabeId);
            return group.stream().filter(m -> !m.equals(abgabeId)).collect(Collectors.toList());
        }

        void copyFileForOtherStudents(NameResult result, Path source, FileSystem zip) {
            abgabeIdToGroup.get(result.moodleID).stream().filter(m -> !m.equals(result.moodleID)).forEach(other -> {
                CSVFile.Student student = csvFile.getStudentForAbgabeId(other);
                try {
                    Files.copy(source, zip.getPath(result.getFileNameForDifferentStudent(student)), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    System.err.println("Failed to copy correction for student " + student);
                }
            });
        }
    }

    static class NameResult {

        String moodleID;
        String lastName;
        String punkte;
        String fileNameInZip;

        public NameResult() {
        }

        public NameResult(String moodleID, String punkte, String fileNameInZip, String lastName) {
            this.moodleID = moodleID;
            this.punkte = punkte;
            this.fileNameInZip = fileNameInZip;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "NameResult{" + "moodleID=" + moodleID + ", lastName=" + lastName + ", punkte=" + punkte + ", fileNameInZip=" + fileNameInZip + '}';
        }

        String getFileNameForDifferentStudent(CSVFile.Student student) {
            int index = this.fileNameInZip.indexOf("_assignsubmission_file_");
            return student.name + "_" + student.abgabeId + this.fileNameInZip.substring(index);
        }
    }

    static NameResult getPunkteFromFileName(File file) {
        String name = file.getName();
        if (name.endsWith(".pdf")) {
            String lastName = null;
            name = name.substring(0, name.length() - 4);
            int lastIndex = name.lastIndexOf('_');
            if (lastIndex < 0) {
                return null;
            }
            name = name.substring(lastIndex + 1);
            name = name.replace(',', '.');
            try {
                Double.parseDouble(name);
            } catch (Exception e) {
                return null;
            }
            //wir haben punkte, check for name
            String moodleID = null;
            String fileNameInZip = null;
            {
                String s = file.getName();
                int index = s.indexOf('_');
                if (!s.contains("_assignsubmission_file_")) {
                    //look for id in parent dir
                    s = file.getParentFile().getName();
                    index = s.indexOf('_');
                    String parentName = file.getParentFile().getName();
                    fileNameInZip = "/" + parentName;
                    lastName = parentName.substring(0, parentName.indexOf(','));
                } else {
                    fileNameInZip = "/";
                    lastName = file.getName().substring(0, file.getName().indexOf(','));
                }
                if (index >= 0) {
                    int counter = 0;
                    index++;
                    while (index < s.length() && Character.isDigit(s.charAt(index))) {
                        index++;
                        counter++;
                    }
                    if (counter >= 5 && counter <= 8) {//valid id
                        moodleID = s.substring(index - counter, index);
                    }
                }
            }
            if (moodleID == null) {
                return null;
            }
            fileNameInZip += file.getName().substring(0, file.getName().lastIndexOf('_')) + ".pdf";
            return new NameResult(moodleID, name, fileNameInZip, lastName);
        }
        return null;
    }

    public static Path findGroupFile(Path start) {
        Path cur = start;
        while (cur != null) {
            if (Files.isReadable(cur.resolve("groups.txt"))) {
                return cur.resolve("groups.txt");
            }
            cur = cur.getParent();
        }
        return null;
    }

    public static String pad(String s, int length) {
        if (s.length() > length) {
            return s.substring(0, length);
        }
        while (s.length() < length) {
            s += " ";
        }
        return s;
    }

    public static Path searchForSubmissionsFiles(Path start, int depth) throws IOException {
        for (Path p : Files.newDirectoryStream(start)) {
            if (p.getFileName().toString().contains("_assignsubmission_file_")) {
                return start;
            }
            if (depth > 0 && Files.isDirectory(p)) {
                Path f = searchForSubmissionsFiles(p, depth - 1);
                if (f != null) {
                    return f;
                }
            }
        }
        return null;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //args = new String[]{"/Users/leanderSchulten/RWTH/Bus 19/Korrektur1/groups.txt", "/Users/leanderSchulten/RWTH/Bus 19/Korrektur1/Bewertungen-BuS-Übungsblatt 1-Übungsgruppe 02-40473.csv", "/Users/leanderSchulten/RWTH/Bus 19/Korrektur1/BuS-Übungsblatt 1-Übungsgruppe 02-40473-1"};
        //args = new String[]{"/Users/leanderSchulten/Downloads/Telegram Desktop/groups.txt", "/Users/leanderSchulten/Downloads/Telegram Desktop/Bewertungen-BuS-Übungsblatt 2-Übungsgruppe 08-47709.csv", "/Users/leanderSchulten/Downloads/Telegram Desktop/BuS-Übungsblatt 2-Übungsgruppe 08-47709"};
        if (args.length == 0) {
            System.out.println("Program startet without command. Showing commands:");
            System.out.println("'alias_as <aliasName>' creates an alias with the name aliasName. Then you can use aliasName to start the Program.");
            System.out.println("'create <optional:path/to/groups.txt> <optional:path/to/Bewertungs*.csv> "
                    + "<optional:path/to/files/>' You can set the optional paths, otherwise the program will search for these files.");
            System.out.println("\tgroups.txt: The file with the groups. If no path is given, the program searches for the 'groups.txt' file");
            System.out.println("\tBewertungs*.csv: The csv file from moodle. If no path is given, the program searches for a file that mathes 'Bewertungs*.csv'");
            System.out.println("\tpath/to/files: The folder where the *_assignsubmission_file_* files or folders are. If no path is given the program search for a folder with these content.");
            System.out.println("\nExecute\ns = '\\n';for(let row of document.getElementsByClassName('unselectedrow')){s += 'name ' + row.children[2].innerText + ' ' + row.children[3].innerText + '\\n'; } s");
            System.out.println("In the console of your browser when you see the submissions to get a list of the users for your groups.txt");
            return;
        }
        String command = args[0];
        args = Arrays.copyOfRange(args, 1, args.length);
        if (command.equalsIgnoreCase("alias_as")) {
            if (args.length > 1) {
                System.err.println("Only provide one name");
                return;
            }
            if (args.length == 0) {
                System.err.println("Please provide a name for the alias");
                return;
            }
            String name = args[0];
            String os = System.getProperty("os.name").toLowerCase();
            String filename = ".bashrc";
            if (os.contains("mac")) {
                filename = ".zshrc";
            }
            String path = PunkteInCSV.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
            String alias = "alias " + name + "='java -jar \"" + path + "\"'";
            Files.write(Paths.get(System.getProperty("user.home"), filename), alias.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
            System.out.println("Execute '. ~/" + filename + "' to use the alias");
            return;
        }
        if (!command.equals("create")) {
            System.err.println("Command '" + command + "' not recognized");
            return;
        }
        Path groupFile = null, files = null, csv = null;
        for (String arg : args) {
            try {
                Path p = Paths.get(arg);
                if (p.getFileName().toString().equalsIgnoreCase("groups.txt")) {
                    System.out.println("Set group file to:" + arg);
                    groupFile = p;
                } else if (Files.isDirectory(p)) {
                    System.out.println("Set directory to:" + arg);
                    files = p;
                } else if (p.getFileName().toString().endsWith(".csv")) {
                    System.out.println("Set CSV file to:" + arg);
                    csv = p;
                } else {
                    System.err.println("Don't know what to do with path:'" + p + "'");
                }
            } catch (InvalidPathException e) {
                System.err.println("Error: Argument '" + arg + "' is not a path.");
            }
        }
        if (files == null) {
            files = searchForSubmissionsFiles(Paths.get("").toAbsolutePath(), 2);
            if (files != null) {
                System.out.println("Use '" + files + "' as folder with submissions.");
            }
        }
        if (groupFile == null) {
            groupFile = findGroupFile(Paths.get("").toAbsolutePath());
            if (groupFile != null) {
                System.out.println("Use group file: " + groupFile);
            } else if (files != null) {
                groupFile = findGroupFile(files.toAbsolutePath());
                if (groupFile != null) {
                    System.out.println("Use group file: " + groupFile);
                } else {
                    System.out.println("No group file found.");
                }
            }
        }
        if (csv == null) {
            for (Path p : Files.newDirectoryStream(Paths.get("."), "Bewertungen*.csv")) {
                if (!p.getFileName().toString().endsWith("_MitBewertung.csv")) {
                    csv = p;
                    System.out.println("Use csv file: " + p);
                    break;
                }
            }
            if (csv == null && files != null) {
                for (Path p : Files.newDirectoryStream(files, "Bewertungen*.csv")) {
                    if (!p.getFileName().toString().endsWith("_MitBewertung.csv")) {
                        csv = p;
                        System.out.println("Use csv file: " + p);
                        break;
                    }
                }
            }
        }

        if (groupFile != null) {
            Files.readAllLines(groupFile).forEach(line -> {
                if (line.startsWith("gro")) {
                    parseGroup(line);
                } else if (line.startsWith("name")) {
                    parseName(line);
                } else if (line.length() > 0 && Character.isDigit(line.charAt(0))) {
                    parsePointsForGroup(line);
                } else if (line.length() > 0) {
                    System.out.println("Line ignored : " + line);
                }
            });
        }

        CSVFile csvFile = null;
        if (csv != null) {
            csvFile = new CSVFile(csv);
        }

        Map<String, String> moodleIDToPunkte = new HashMap<>();
        List<NameResult> results = new ArrayList<>();
        //Check Files
        if (files != null) {
            // see https://docs.oracle.com/javase/7/docs/technotes/guides/io/fsp/zipfilesystemprovider.html
            Map<String, String> env = new HashMap<>();
            env.put("create", "true");
            File rootDir = files.toFile();
            if (!rootDir.exists() || !rootDir.isDirectory()) {
                System.err.println("Das Verzeichnis : \"" + rootDir.getAbsolutePath() + "\" existiert nicht oder ist kein Ordner.");
                return;
            }
            rootDir = rootDir.getCanonicalFile();
            if (rootDir.getParentFile() == null) {
                System.err.println("Der Ordner \"" + rootDir.getAbsolutePath() + "\" hat kein Vater Ordner.");
                return;
            }
            if (rootDir.getParentFile().toURI() == null) {
                System.err.println("Der Ordner \"" + rootDir.getParentFile().getAbsolutePath() + "\" kann nicht zu einer URI konvertiert werden.");
                return;
            }
            Optional<AbgabeIdGroups> abgabeIdGroups = Optional.empty();
            if (csvFile != null) {
                abgabeIdGroups = Optional.of(new AbgabeIdGroups(groups, csvFile));
            }
            URI uri = URI.create("jar:" + rootDir.getParentFile().toURI() + "zipForMoodle.zip");
            FileSystem zipfs = FileSystems.newFileSystem(uri, env);
            for (File file : rootDir.listFiles()) {
                if (file.isFile()) {
                    NameResult result = getPunkteFromFileName(file);
                    if (result != null) {
                        Path source = Paths.get(file.getAbsolutePath());
                        Files.copy(source, zipfs.getPath(result.fileNameInZip), StandardCopyOption.REPLACE_EXISTING);
                        moodleIDToPunkte.put(result.moodleID, result.punkte);
                        System.out.println(result);
                        results.add(result);
                        abgabeIdGroups.ifPresent(g -> g.copyFileForOtherStudents(result, source, zipfs));
                    }
                } else if (file.isDirectory()) {
                    for (File subFile : file.listFiles()) {
                        if (subFile.isFile()) {
                            NameResult result = getPunkteFromFileName(subFile);
                            if (result != null) {
                                System.out.println(result);
                                Path source = Paths.get(subFile.getAbsolutePath());
                                Files.copy(source, zipfs.getPath(result.fileNameInZip), StandardCopyOption.REPLACE_EXISTING);
                                moodleIDToPunkte.put(result.moodleID, result.punkte);
                                results.add(result);
                                abgabeIdGroups.ifPresent(g -> g.copyFileForOtherStudents(result, source, zipfs));
                            }
                        }
                    }
                }
            }
            zipfs.close();
            if (csvFile != null) {
                for (CSVFile.Student student : csvFile.getStudents()) {
                    if (moodleIDToPunkte.containsKey(student.abgabeId)) {
                        punkte.put(getGroupForMatrNummer(student.matrikelnummer), moodleIDToPunkte.get(student.abgabeId));
                    }
                }
            }
        }

        //erstelle csv mit ergebnis
        checkGroups();
        if (csv != null && groupFile != null && files != null) {
            csvFile.write(PunkteInCSV::getPointsForMatrNummer);
        } else if (groupFile != null && files != null) {
            if (matrForName.size() == 0) {
                System.out.println("If you provide 'name <lastName, firstName> <matrNum>' lines in the groups.txt file, a mapping from matrNums to points can be created");
                System.out.println("\nExecute\ns = '\\n';for(let row of document.getElementsByClassName('unselectedrow')){s += 'name ' + row.children[2].innerText + ' ' + row.children[3].innerText + '\\n'; } s");
                System.out.println("In the console of your browser when you see the submissions to get a list of all users for your groups.txt");
            } else {
                System.out.println("Create mapping from matr number to points");
                List<String> lines = new ArrayList<>();
                Map<String, String> matToName = new HashMap<>(fullNameForMatrNum);
                for (NameResult result : results) {
                    punkte.put(getGroupForName(result.lastName), result.punkte);
                }
                for (Set<String> group : groups) {
                    String points = punkte.getOrDefault(group, "Keine Punkte bekommen");
                    group.forEach(m -> lines.add(m + " " + pad(fullNameForMatrNum.getOrDefault(m, ""), 20) + "(" + group.size() + ") : " + points));
                    group.forEach(matToName::remove);
                }
                int totalWithPoints = lines.size();
                matToName.forEach((mat, name) -> lines.add(mat + " " + pad(name, 20) + "    : No submission, no group"));
                Collections.sort(lines);
                lines.forEach(System.out::println);
                System.out.println("Total: " + lines.size());
                System.out.println("Total with points: " + totalWithPoints);

            }
        }

    }

}
